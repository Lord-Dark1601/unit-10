import java.io.*;

public class NifException extends IOException {

	public NifException() {
		super();
	}

	public NifException(String msg) {
		super(msg);
	}
}
