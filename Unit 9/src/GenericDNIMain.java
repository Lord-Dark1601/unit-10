
public class GenericDNIMain {

	public static void main(String[] args) throws NifException {

		SortedArray<GenericDNI> arr = new SortedArray<GenericDNI>(10);

		arr.putProfe(new GenericDNI(12345676));
		arr.putProfe(new GenericDNI(12345677));
		arr.putProfe(new GenericDNI(12345678));
		arr.putProfe(new GenericDNI(12345679));

		for (int i = 0; i < arr.getNunElements(); i++) {
			System.out.println(arr.getElementAt(i).toFormattedString());
		}

	}

}
