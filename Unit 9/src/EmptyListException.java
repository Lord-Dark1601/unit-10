
public class EmptyListException extends ItemNotFoundException {

	public EmptyListException() {
		super("Empty list Exception");
	}

	public EmptyListException(String msg) {
		super(msg);
	}
}
