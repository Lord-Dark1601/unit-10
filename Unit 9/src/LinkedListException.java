
public class LinkedListException extends Exception {

	public LinkedListException() {
		super("Linked list Exception");
	}

	public LinkedListException(String msg) {
		super(msg);
	}
}
