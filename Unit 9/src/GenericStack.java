import java.util.*;

public class GenericStack<E> {

	private ArrayList<E> list;

	public GenericStack() {
		list = new ArrayList<E>();
	}

	public void push(E inserted) {
		list.add(0, inserted);
	}

	public E pop() {
		E item = list.get(0);
		list.remove(0);
		return item;
	}
	
	public boolean isEmpty() {
		return list.get(0)==null;
	}
	
	public void empty() {
		list.clear();
	}
}
