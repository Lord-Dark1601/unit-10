
public class LinkedList<E> {

	private Element<E> first;
	private Element<E> last;

	public LinkedList() {
		first = null;
		last = null;
	}

	public void insertFirst(E obj) {
		Element<E> e = new Element<E>(obj);
		if (first == null) {
			last = e;
		} else {
			e.setNext(first);
		}
		first = e;
	}

	public void insertLast(E obj) {
		Element<E> e = new Element<E>(obj);
		if (last == null) {
			first = e;
		} else {
			last.setNext(e);
		}
		last = e;
	}

	public void print() {
		Element<E> e = first;
		while (e != null) {
			System.out.print(e.getItem() + " ");
			e = e.getNext();
		}
		System.out.println();
	}

	public boolean isEmpty() {
		if (first == null) {
			return true;
		} else {
			return false;
		}
	}

	public void remove(E object) throws ItemNotFoundException, EmptyListException {

		if (isEmpty()) {
			throw new EmptyListException();
		}

		Element<E> current, previous;

		current = first;
		previous = first;

		while (current != null && !current.getItem().equals(object)) {
			previous = current;
			current = current.getNext();
		}
		if (current == null) {
			throw new ItemNotFoundException("Element " + object + " not found in the list");
		}

		if (first == last) {
			first = null;
			last = null;
		} else {
			if (current == first) {
				first = current.getNext();
			} else {
				previous.setNext(current.getNext());
			}
			if (current == last) {
				last = previous;
			}
		}

	}

	public E getFirstItem() throws EmptyListException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		return first.getItem();
	}

	public E getLastObject() throws EmptyListException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		return last.getItem();
	}

	public int getNumElements() {
		Element<E> e = first;
		int count = 0;
		while (e != null) {
			count++;
			e = e.getNext();
		}
		return count;
	}

	public E getItemAtPosition(int i) throws EmptyListException, InvalidIndexException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		Element<E> e = first;
		int count = 1;
		while (e != null) {
			if (count == i) {
				return e.getItem();
			} else {
				e = e.getNext();
				count++;
			}
		}
		if (i >= count) {
			InvalidIndexException in = new InvalidIndexException("The position is bigger than the index");
			throw in;
		}
		throw new Error("This should never happen, if happends, we are fucked");
	}
}
