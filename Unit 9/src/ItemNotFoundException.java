
public class ItemNotFoundException extends LinkedListException {

	public ItemNotFoundException() {
		super("Object not found Exception");
	}

	public ItemNotFoundException(String msg) {
		super(msg);
	}
}
