
public class Stack<E> {

	private LinkedList<E> l;

	public Stack() {
		l = new LinkedList<E>();
	}

	public void push(E obj) {
		l.insertFirst(obj);
	}

	public E pop() throws EmptyListException, ItemNotFoundException {
		E item = l.getFirstItem();
		l.remove(l.getFirstItem());
		return item;
	}

	public boolean isEmpty() {
		if (l.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	public void empty() throws EmptyListException, ItemNotFoundException {
		int numObj = l.getNumElements();
		for (int i = 0; i < numObj; i++) {
			l.remove(l.getFirstItem());
		}
	}

	public void print() {
		l.print();
	}
}
